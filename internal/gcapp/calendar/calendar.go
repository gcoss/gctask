package calendar

import (
	"fmt"
	"strconv"
	"time"

	"fyne.io/fyne"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
)

func monthContainer(t time.Time) *fyne.Container {
	calendarWithTitle := fyne.NewContainerWithLayout(layout.NewVBoxLayout())

	monthTitleCanvas := widget.NewLabel(fmt.Sprintf("%v - %v", t.Year(), t.Month().String()))
	updateMonthTitleCanvas := func(t time.Time) {
		monthTitleCanvas.SetText(fmt.Sprintf("%v - %v", t.Year(), t.Month().String()))
		// monthTitleCanvas.Refresh()
	}

	var daysCanvas []*widget.Label
	var month *fyne.Container

	updateDaysCanvas := func(t time.Time) {
		var c []*widget.Label
		monthStartWeekday := t.AddDate(0, 0, (t.Day()*-1)+1).Weekday()
		offset := 0

		daysOfTheWeek := []string{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}

		for i := 0; i < len(daysOfTheWeek); i++ {
			if monthStartWeekday.String() == daysOfTheWeek[i] {
				calcOffset := offset + i
				for i := 0; i < calcOffset; i++ {
					c = append(c, widget.NewLabel(""))
				}
			}
		}

		currentYear, currentMonth, _ := t.Date()
		currentLocation := t.Location()

		firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
		lastOfMonth := firstOfMonth.AddDate(0, 1, -1)

		daysInMonth := lastOfMonth.YearDay() - firstOfMonth.YearDay() + 1

		for i := 1; i <= daysInMonth; i++ {
			c = append(c, widget.NewLabel(strconv.Itoa(i)))
		}
		daysCanvas = c
	}

	titles := []*widget.Label{
		widget.NewLabel("Sun"),
		widget.NewLabel("Mon"),
		widget.NewLabel("Tue"),
		widget.NewLabel("Wed"),
		widget.NewLabel("Thu"),
		widget.NewLabel("Fri"),
		widget.NewLabel("Sat"),
	}

	updateDaysCanvas(t)

	calendar := fyne.NewContainerWithLayout(
		layout.NewGridLayout(7),
	)

	for _, v := range append(titles, daysCanvas...) {
		calendar.AddObject(v)
	}

	updateCalendar := func() {
		calendar = fyne.NewContainerWithLayout(
			layout.NewGridLayout(7),
		)

		for _, v := range append(titles, daysCanvas...) {
			calendar.AddObject(v)
		}
		calendar.Refresh()
		calendarWithTitle.Objects = []fyne.CanvasObject{}
		calendarWithTitle.AddObject(month)
		calendarWithTitle.AddObject(calendar)
		calendarWithTitle.Refresh()
	}

	subMonth := widget.NewButton("<", func() {
		t = t.AddDate(0, -1, 0)
		updateMonthTitleCanvas(t)
		updateDaysCanvas(t)
		updateCalendar()
	})
	addMonth := widget.NewButton(">", func() {
		t = t.AddDate(0, 1, 0)
		updateMonthTitleCanvas(t)
		updateDaysCanvas(t)
		updateCalendar()
	})

	month = fyne.NewContainerWithLayout(
		layout.NewHBoxLayout(),
		subMonth,
		layout.NewSpacer(),
		monthTitleCanvas,
		layout.NewSpacer(),
		addMonth,
	)

	calendarWithTitle.AddObject(month)
	calendarWithTitle.AddObject(calendar)
	return calendarWithTitle
}

func loadCalendarGroup() fyne.Widget {
	otherGroup := widget.NewGroup("Calendar",
		monthContainer(time.Now()),
	)
	return widget.NewVBox(otherGroup)
}

// MainScreen shows a panel containing network demos
func MainScreen() fyne.CanvasObject {
	return fyne.NewContainerWithLayout(layout.NewMaxLayout(),
		widget.NewVScrollContainer(loadCalendarGroup()),
	)
}
