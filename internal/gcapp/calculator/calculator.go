package calculator

import (
	"strconv"

	"fyne.io/fyne"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
)

func calcContainer() *fyne.Container {

	results := widget.NewLabel("0")
	pendingOperation := ""

	storedValue := 0.0

	operate := func(buttonPushed string) {
		switch buttonPushed {
		case "C":
			storedValue = 0.0
			results.SetText("0")
		case "+", "-":
			v, err := strconv.ParseFloat(results.Text, 64)
			if err != nil {
				storedValue = 0.0
				results.SetText("0")
			}
			storedValue = v
			results.SetText("0")
			pendingOperation = buttonPushed
		case "=":
			v, err := strconv.ParseFloat(results.Text, 64)
			if err != nil {
				storedValue = 0
				results.SetText("")
			}
			switch pendingOperation {
			case "+":
				results.Text = strconv.FormatFloat(storedValue+v, 'G', -1, 64)
			case "-":
				results.Text = strconv.FormatFloat(storedValue-v, 'f', -1, 64)
			default:
			}
		default:
			if results.Text == "0" {
				results.Text = buttonPushed
			} else {
				results.Text = results.Text + buttonPushed
			}
		}

		results.Refresh()
	}

	buttons := []*widget.Button{
		widget.NewButton("1", func() { operate("1") }),
		widget.NewButton("2", func() { operate("2") }),
		widget.NewButton("3", func() { operate("3") }),
		widget.NewButton("4", func() { operate("4") }),
		widget.NewButton("5", func() { operate("5") }),
		widget.NewButton("6", func() { operate("6") }),
		widget.NewButton("7", func() { operate("7") }),
		widget.NewButton("8", func() { operate("8") }),
		widget.NewButton("9", func() { operate("9") }),
		widget.NewButton(".", func() { operate(".") }),
		widget.NewButton("0", func() { operate("0") }),
		widget.NewButton("=", func() { operate("=") }),
		widget.NewButton("+", func() { operate("+") }),
		widget.NewButton("-", func() { operate("-") }),
		widget.NewButton("C", func() { operate("C") }),
	}

	buttonsContainer := fyne.NewContainerWithLayout(layout.NewGridLayout(3))
	for i := 0; i < len(buttons); i++ {
		buttonsContainer.AddObject(buttons[i])
	}

	buttonsAndResultsContainer := fyne.NewContainerWithLayout(layout.NewVBoxLayout(),
		fyne.NewContainerWithLayout(layout.NewHBoxLayout(), layout.NewSpacer(), results),
		buttonsContainer,
	)

	return buttonsAndResultsContainer
}

func loadCalendarGroup() fyne.Widget {
	calcGroup := widget.NewGroup("Calculator", calcContainer())
	return widget.NewVBox(calcGroup)
}

// MainScreen shows a panel containing network demos
func MainScreen() fyne.CanvasObject {
	return fyne.NewContainerWithLayout(layout.NewMaxLayout(),
		widget.NewVScrollContainer(loadCalendarGroup()),
	)
}
