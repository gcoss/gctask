package network

import (
	"fyne.io/fyne"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
	"gitlab.com/gcoss/gctask/internal/pkg/misc"
)

func makeSimUnlockForm() fyne.Widget {
	path := widget.NewEntry()
	path.SetPlaceHolder("http://192.168.1.1/")
	path.SetText("http://192.168.2.1/")
	pin := widget.NewEntry()
	pin.SetPlaceHolder("0000")
	pin.SetText("0000")
	password := widget.NewPasswordEntry()
	password.SetPlaceHolder("Password")
	password.SetText("Password")

	form := &widget.Form{
		Items: []*widget.FormItem{
			{Text: "Path", Widget: path},
			{Text: "Pin", Widget: pin},
		},
		OnCancel: func() {
			path.SetText("")
			pin.SetText("")
			password.SetText("")
		},
		OnSubmit: func() {
			n := &fyne.Notification{
				Title:   "SIM Unlock",
				Content: "Working...",
			}
			fyne.CurrentApp().SendNotification(n)
			err := misc.SendSimPin(path.Text, pin.Text, password.Text)
			if err != nil {
				n.Title = "SIM Unlock ERROR"
				n.Content = err.Error()
				fyne.CurrentApp().SendNotification(n)
			} else {
				n.Title = "SIM Unlocked"
				n.Content = "Hopefully :)"
				fyne.CurrentApp().SendNotification(n)
			}
		},
	}
	form.Append("Password", password)
	return form
}

func loadMiscGroup() fyne.Widget {
	otherGroup := widget.NewGroup("SIM Unlock",
		makeSimUnlockForm(),
	)

	return widget.NewVBox(otherGroup)
}

// MainScreen shows a panel containing network demos
func MainScreen() fyne.CanvasObject {
	return fyne.NewContainerWithLayout(layout.NewAdaptiveGridLayout(1),
		widget.NewScrollContainer(loadMiscGroup()))
}
