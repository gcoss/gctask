package main

import (
	"fmt"

	"gitlab.com/gcoss/gctask/internal/gcapp"
	"gitlab.com/gcoss/gctask/internal/pkg/hello"
)

func main() {
	fmt.Println(hello.Hello())
	gcapp.Start()
}
