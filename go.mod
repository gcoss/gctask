module gitlab.com/gcoss/gctask

go 1.13

require (
	fyne.io/fyne v1.3.3
	github.com/Kodeworks/golang-image-ico v0.0.0-20141118225523-73f0f4cfade9
	github.com/jstemmer/go-junit-report v0.9.1 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/t-yuki/gocover-cobertura v0.0.0-20180217150009-aaee18c8195c // indirect
	golang.org/x/tools v0.0.0-20200911193555-6422fca01df9 // indirect
)
