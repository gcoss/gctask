# Set PROJECT_DIR to the project root directory
APP_ID=com.cornco.gctask

# fyne-cross build android
fyne-cross android -app-id $APP_ID

# install via adb
adb install -r fyne-cross/dist/android/gctask.apk